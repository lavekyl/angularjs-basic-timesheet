function timesheetCtrl($scope) {
    $scope.items = [{
        Name: "K Laverty",
        Project: "Project 1",
        Mon: "8",
        Tues: "",
        Wed: "",
        Thur: "4",
        Fri: "4"
    }];

    $scope.addItem = function (item) {
        $scope.items.push(item);
        $scope.item = {};
    };

    $scope.removeItem = function (index) {
        $scope.items.splice(index, 1);
    };
}